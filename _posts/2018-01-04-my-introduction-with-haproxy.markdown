---
layout: post
title:  "My introduction with HAProxy"
date:   2018-01-04 01:54:00 +0700
categories: haproxy
tags: system haproxy reverse-proxy load-balance
---

Before i knew about [HAproxy](http://www.haproxy.org), i always use [Varnish](https://varnish-cache.org/) to be a [Reverse Proxy](https://en.wikipedia.org/wiki/Reverse_proxy).
But there are some [differences](https://www.haproxy.com/blog/haproxy-and-varnish-comparison/) between them, Varnish is most suitable for [caching](https://www.maxcdn.com/one/visual-glossary/proxy-caching/) purpose.
So i decide to try HAProxy this time for my goal :)

-----

### Objectives
* Have a reverse proxy for multiple services, route by domains, sub-domains and URL path
* ~~Have a valid SSL~~ *will be discussed in part 2*

### HAProxy
The first thing we must do is install HAProxy. To install HAProxy is quite straight forward, i use Ubuntu 16.04 as my box. So i just need run `apt-get` command to install it.
```bash
$ sudo apt-get update
$ sudo apt-get install haproxy
```

*Note: Please refer to [HAProxy site](http://www.haproxy.org) to install in different environment.*

And make sure HAProxy is running by

```bash
$ haproxy -v
```

HAProxy configuration is stored in file `/etc/haproxy/haproxy.cfg`, please take a moment to look at it :)

### Frontend and Backend
I have a domain named `somecooldomain.com` and serve web content from 1 service only. But what if in the future i decide to add a sub domain for specific service?
So i will have `myapp.somecooldomain.com` to serve API from `myapp` service. We need to add more layer in front of our services to achieve that.

----


Basically a reverse proxy will act as `frontend` and your services will known as `backend`.
Every request to `frontend` will be filtered and matched based on a simple (or complex) rule known as [ACL](https://en.wikipedia.org/wiki/Access_control_list).
We can define ACL by criteria, ex: domain, URL path, method, etc. Please see [here](https://www.haproxy.com/documentation/aloha/7-0/traffic-management/lb-layer7/acls/) for HAProxy ACls.


#### Frontend
In my case, the ACL for `frontend` will look like this :

```bash
frontend frontend-http
    acl acl-web hdr_dom(host) -i somecooldomain.com
    acl acl-myapp hdr_dom(host) -i myapp.somecooldomain.com
```

What happen here is we separate ACL between main domain and its sub domain. `hdr_dom` criterion will take a domain match for its rule.
This is required since we need to tell HAProxy only process request from specific domain.

And this part will take request to appropriate `backend`

```bash
    use_backend backend-web if acl-web
    use_backend backend-myapp if acl-myapp
```

Also we can tell to use `default backend` if ACL rules not match

```bash
    default_backend backend-web
```

#### Backend
We use `backend` section to configure our services

```bash
backend backend-web
    option httpchk GET /
    server backend-web-1 127.0.0.1:8080 check fall 3 rise 2 inter 5s
```

`option httpchk GET /` is a [Health Check](https://www.haproxy.com/documentation/aloha/7-0/traffic-management/lb-layer7/health-checks/) to determine our service is up.
HAProxy will do a request with certain `method` (in this case is a `GET`) to specific URL.

*Note: Every changes made on `haproxy.cfg` do `$ sudo service haproxy restart` to apply it into HAProxy*

----

There are a lot of configuration examples for setting up HAProxy, just look into [Google](https://www.google.com) and try to mix and match based on your need :)