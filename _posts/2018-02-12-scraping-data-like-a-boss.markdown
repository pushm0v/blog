---
layout: post
title:  "Scraping Data Like a Boss"
date:   2018-02-12 18:59:00 +0700
categories: java android scraping web api security
tags: java android scraping web api security
---

*Disclaimer : All information or sensitive data that exposed in this article
will be censored*

One aspect that sometime you overlooked when creating your API is prevent
someone else do a automate data scraping on your database. We always can use
token based authentication to add extra security layer or even use an API gateway
to rate-limiting request to our API.

On my journey to scrape data from some websites, i found various method on how
the developer trying to prevent scraping. Some of them add a *hash* check using
SHA256 algorithm. But this method will be useless if you can find the *secret key*.


There are many source to scrape data from your target, one of them is website.
All information are displayed in plain text and you can run a HTML parser to get it.
Even some API only can be accessed via mobile app.

I have to scrape data from `YYYY` mobile app, we have to use MITM attack to get
the API. [Charles Proxy](https://www.charlesproxy.com/) is one of the best tool to do this. It can *fake* SSL certificate
on simulator or browser. Of course you need an Android emulator to do this.
Get a [Genymotion](https://www.genymotion.com/) and create a new phone on it, then run
Charles Proxy.

![charles-proxy charles-proxy](/blog/assets/img/charles-proxy-1.jpg)


We can see that for every request to API, there are `X-yyyy-Random-Key`, `X-yyyy-Access-Timestamp` & `X-yyyy-Access-Token` headers.
Those values are dynamically generated for each request. So we can assume there is some
kind of logic behind these value.

Lets decompile the APK first, we can download the APK from [APKPure](https://apkpure.com/
). Just search the app name or its identifier string (ex: com.yyyy.app).
Rename the apk file into `.zip` and extract it.

![apk-extracted apk-extracted](/blog/assets/img/apk-extracted.jpg){:height="320px" width="160px"}

Our target is `*.dex` files which contain Android compiled code.
I'm using [dex2jar](https://github.com/pxb1988/dex2jar) tool to convert it into `*.jar` file.
The converted file can be opened using [JD GUI](https://github.com/java-decompiler/jd-gui).

![jd-gui jd-gui](/blog/assets/img/jd-gui-1.jpg){:height="320px" width="160px"}

We can see a lot of information here, lets focus on our target package. There is one class that caught my eyes, it is `ApiSecretKeyConfigs`

![jd-gui jd-gui](/blog/assets/img/jd-gui-2.jpg)


```java
package com.yyyy.app;

public class ApiSecretKeyConfigs
{
  static
  {
    System.loadLibrary("keys");
  }

  public static String getHMAC_SHA256_SECRET_KEY()
  {
    // ....
        str = getNativeKey7();
    // ....
  }
// The rest of code continue here...
```

It call `getNativeKey7()` function to get the *secret key* from
shared object file. Notice `System.loadLibrary("keys");` statement, this code looks
`keys.so` file to be loaded.

So lets get back into APK folder structure, there is `lib` folder that contains
all `*.so` based on platform.

![shared-object shared-object](/blog/assets/img/shared-object.jpg)

We can use this file directly from `Android` code or you can disassembly it first using `IDA Pro` or
online tools like [ODA Web](http://onlinedisassembler.com/odaweb/).

Here is a snippet of android code that i use, dont forget to place `libkeys.so` into `jniLibs` folder :


```java

// MainActivity.java

package com.yyyy.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApiSecretKeyConfigs api = new ApiSecretKeyConfigs();
        String key = api.getHMAC_SHA256_SECRET_KEY();
        Log.e("SECRET_KEY",key);
    }
}

```

```java

// ApiSecretKeyConfigs.java

public class ApiSecretKeyConfigs {
    static {
        System.loadLibrary("keys");
    }

    public static String getHMAC_SHA256_SECRET_KEY()
    {
        return getNativeKey7();
    }
}

```

Are we done? Of course not :) We need to figure out how *random key* take a part
in the algorithm. Switch to JD GUI, and try to find `getHMAC_SHA256_SECRET_KEY` string
being used.

![jd-gui jd-gui](/blog/assets/img/jd-gui-3.jpg)

There you go, we got another class to inspect, `CloudUtils`.

```java

public static Map<String, String> initExtraHeaders(Map<String, String> paramMap, String paramString1, String paramString2, String paramString3)
{
    // ...

        paramMap = new java/text/SimpleDateFormat;
        paramMap.<init>("yyyy-MM-dd HH:mm:ss");
        paramMap.setTimeZone(TimeZone.getTimeZone("GMT"));
        localObject2 = new java/util/Date;
        ((Date)localObject2).<init>();
        localObject2 = paramMap.format((Date)localObject2);
        paramMap = FUtils.getRandomString(10);
        ((Map)localObject1).put(CloudConst.YYYY_ACCESS_TIMESTAMP, localObject2);
        ((Map)localObject1).put(CloudConst.YYYY_RANDOM_KEY, paramMap);
        if (!"GET".equals(paramString2)) {
          break label405; // Goto label405 if POST method
        }
        paramString2 = new java/lang/StringBuilder;
        paramString2.<init>();
        paramMap = paramString1 + paramMap + (String)localObject2;
        ((Map)localObject1).put(CloudConst.YYYY_ACCESS_TOKEN, FUtils.encode_HMAC_SHA256(ApiSecretKeyConfigs.getHMAC_SHA256_SECRET_KEY(), paramMap));

      for (;;)
      {
        ((Map)localObject1).put("Cache-Control", "no-store, no-cache, must-revalidate");
        return (Map<String, String>)localObject1;
        label405:
        paramString2 = new java/lang/StringBuilder;
        paramString2.<init>();
        paramMap = paramString1 + paramString3 + paramMap + (String)localObject2;
        ((Map)localObject1).put(CloudConst.YYYY_ACCESS_TOKEN, FUtils.encode_HMAC_SHA256(ApiSecretKeyConfigs.getHMAC_SHA256_SECRET_KEY(), paramMap));
      }

     // ...
}

```

`initExtraHeaders` is the function to add headers when making a network call. It will add
`YYYY_ACCESS_TIMESTAMP`, `YYYY_RANDOM_KEY` & `YYYY_ACCESS_TOKEN` headers. My guess is `YYYY_ACCESS_TOKEN`
is generated from timestamp & random key calculated into a hash. Random key is a 10 chars
random returned from `FUtils.getRandomString(10)`. Timestamp should in the `GMT` timezone.

Also there is a condition based on what HTTP method that its used. I bet it will append request
parameters in hash calculation :)

```java

if (!"GET".equals(paramString2)) {

//...

}

```

Next, take a look `encode_HMAC_SHA256` function, search all occurrence in JD GUI. And we got :

```java

public static String encode_HMAC_SHA256(String paramString1, String paramString2)
    throws Exception
{
    Mac localMac = Mac.getInstance("HmacSHA256");
    localMac.init(new SecretKeySpec(paramString1.getBytes(), "HmacSHA256"));
    return Base64.encodeToString(localMac.doFinal(paramString2.getBytes("UTF-8")), 0);
}

```

It is returning `Base64` encoded string from SHA256 hash calculation.
The final alogrithm would be :

```java

String payload = "https://api.yyyy.com/api/blablabla" + REQUEST_PARAMETERS + 10_RANDOM_CHARS + TIMESTAMP_YYYY_MM_DD_HH:MM:SS

```

Bingo..!!!

```java

ApiSecretKeyConfigs api = new ApiSecretKeyConfigs();
String key = api.getHMAC_SHA256_SECRET_KEY();
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
Date date = new Date();
String dateString = dateFormat.format(date);
String randomString = FUtils.getRandomString(10);
String request = "SOME_REQUEST_BODY";
String payload = "https://api.yyyy.vn/api/" + request + randomString + dateString;

String accessToken = api.encode_HMAC_SHA256(key, payload))
Log.e("ACCESS_TOKEN",accessToken);

```

And we can use `accessToken` in each request :)