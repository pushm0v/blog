---
layout: post
title:  "Proxy Rotator Gem"
date:   2018-01-13 13:17:00 +0700
categories: ruby gem proxy proxyrotator
tags: ruby gem proxy proxyrotator
---

I have a task to crawl a site and scrape data as many as i can. Unfortunately this site has a rate limiter.
After some trial and error, i found that it will limit your X requests in Y minutes and blocked your IP address for some time.
It only can be bypassed by changing your IP address constantly using a [Proxy](https://en.wikipedia.org/wiki/Proxy_server).

After searching in [Google](https://www.google.com), There are many proxy services available and i decide to try [ProxyRotator](https://www.proxyrotator.com).
ProxyRotator offer an API to use its service and this is what i need. Each API call will give you a random proxy to use.

At first, i just wrote a code to call ProxyRotator API and feed my REST client to use it. But at some time, i need to use my own proxies list too.
So i decide to write my own gem to achieve that.

Source code available [Here](https://gitlab.com/pushm0v/proxy_rotator), Feel free to fork it :)

---

Basically `proxy_rotator` gem will call the API using `rest-client` with certain `API Key`. You can configure what proxy that you want to get.
If you have your own proxies, you can load it and do a check if the proxy can forward your request or not.

## Installation

Add this line to your `Gemfile`:

```ruby
gem 'proxy_rotator'
```

And do `bundle install`

## Usage

Include `proxy_rotator` gem in your `.rb` file
```ruby
require 'proxy_rotator'
```

## Configuration

```ruby
ProxyRotator.configure do |config|
  config.default_timeout = 5 # 5 seconds
  config.api_key = 'YOUR_PROXY_ROTATOR_API_KEY'
  config.default_test_url = 'https://google.com'
end
```

### Use proxyrotator.com service

```ruby
my_proxy = ProxyRotator.rotate_remote
```

Passing API parameters
```ruby
get	true/false	Proxy supports GET requests
post	true/false	Proxy supports POST requests
cookies	true/false	Proxy supports cookies
referer	true/false	Proxy supports referer header
userAgent	true/false	Proxy supports user-agent header
port	integer	Return only proxies with specified port
city	string	Return only proxies with specified city
state	string	Return only proxies with specified state
country	string	Return only proxies with specified country
xml	true/false	Response will be in XML instead of jSON
```

Example
```ruby
config = {
    :get => true,
    :port => 8080,
    :city => 'New York',
    :state => 'NY',
    :country => 'US'
}

my_proxy = ProxyRotator.rotate_remote(config)
```

Get proxy information

```ruby
proxy_info = ProxyRotator.describe_remote(config)
```

### Load proxy list from a file

Example :

```ruby
http://username:password@1.2.3.4:5678
http://1.2.3.4:5678
```

```ruby
ProxyRotator.load_file('/your/proxy-list.txt')
my_proxy = ProxyRotator.rotate
```

#### Check proxy with test url

```ruby
my_proxy = ProxyRotator.rotate(true)
```

### Combine remote and rotate

```ruby
my_proxy = ProxyRotator.rotate_first_then_remote
```

```ruby
my_proxy = ProxyRotator.remote_first_then_rotate
```


